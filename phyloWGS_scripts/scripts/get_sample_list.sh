#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to creat a TSV file with two columns (sample_id and location_id), which maps the name of samples to continuous integer IDs starting with 0.
#####################################################

set -e
set -u

patient=$1    # The ID of the patient to analyze
dir_titan=$2    # The directory that stores the results of TitanCNA for all patients
dir_phylowgs=$3   # The directory that stores the results of PhyloWGS for all patients
fout=$4   # The name of output file, which is a TSV file with two columns (sample_id and location_id)

# Example values of the arguments
# patient=PLANET_C002
# dir_titan=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/titan
# dir_phylowgs=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs
# fout=all_sample.txt

tdir=$dir_titan/patient_"$patient"
echo $tdir
sdir=$dir_phylowgs/patient_"$patient"

echo -e "sample_id\tlocation_id" > $fout

# Suppose file $tdir/sample_"$patient exists, which contains samples for the selected patient (one column with sample names)
samples=""
# Read all the samples into an array
for i in `cat $tdir/sample_"$patient"`; do
   if [[ -z $samples ]]; then
      samples="$i"
   else
      samples="$samples,$i"
   fi
done
echo $samples
IFS=',' read -a array <<< "$samples"
for index in "${!array[@]}"
do
    # Skip the first normal sample
    if [ $index -gt 0 ]; then
      i=$(echo "$index-1" | bc)
      echo -e "$i\t${array[index]}" >> $fout
    fi
done

