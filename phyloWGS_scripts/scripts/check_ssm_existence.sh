#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to check the existence of ssm_data.txt for PyClone input.
# Output: Files containing list of patients to prepare ssm_data.txt for PyClone.
#####################################################


fpatient=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patients57
odir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs
fout1=$odir/2run_wesall
fout2=$odir/2run_s500all

cat /dev/null > $fout1
while read patient; do
  # echo $patient
  dir=patient_$patient
  fssm=$dir/WES/wes_all/$patient/ssm_data.txt
  wc -l $fssm
  # wc -l $dir/WES/wes_default/$patient/ssm_data.txt
  if [ ! -f $fssm ]; then
    echo $patient >> $fout1
  fi
done < $fpatient

cat /dev/null > $fout2
while read patient; do
  # echo $patient
  # patient=16-003799
  dir=patient_$patient
  fssm=$dir/WGS/ssm_500_all/*/ssm_data.txt
  wc -l $fssm
  if [ ! -f $fssm ]; then
    # rm -rf $dir/WGS/.snakemake
    echo $patient >> $fout2
  fi
done < $fpatient