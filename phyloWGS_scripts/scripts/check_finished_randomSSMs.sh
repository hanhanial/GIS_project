#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to find patients finishing the run of PhyloWGS on variants randomly selected on the whole genome.
#####################################################

set -e
set -u

fpatient=$1
# fpatient=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patients57
wdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV

cd $wdir

# Record patients not finishing the run
cat /dev/null > wgsall_err
# Record patients finishing the run
cat /dev/null > wgsall_done
while read patient; do
  # echo $patient
  dir=patient_$patient
  fres=$dir/WGS/ssm_500_all/$patient/*html
  # fres=$dir/WGS/ssm_500_all/$patient/*ssm_500_all_files
  # ll $dir/WGS/ssm_500_all/$patient/
  # wc -l $dir/WGS/wgs_default/$patient/ssm_data.txt
  if [ ! -f $fres ]; then
    echo $patient >> wgsall_err
  else
    # ll $fres
    echo $patient >> wgsall_done
  fi
done < $fpatient
