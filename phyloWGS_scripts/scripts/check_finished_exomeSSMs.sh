#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to find patients finishing the run of PhyloWGS on variants on exome region.
#####################################################

set -e
set -u

fpatient=$1
# fpatient=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patients57
wdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV

cd $wdir

# Record patients not finishing the run
cat /dev/null > wesall_err
# Record patients finishing the run
cat /dev/null > wesall_done
while read patient; do
  # echo $patient
  dir=patient_$patient
  fres=$dir/WES/wes_all/$patient/*html
  # fres=$dir/WES/wes_all/$patient/*wes_all_files
  # wc -l $dir/WES/wes_default/$patient/ssm_data.txt
  if [ ! -f $fres ]; then
    echo $patient >> wesall_err
  else
    # ll $fres
    echo $patient >> wesall_done
  fi
done < $fpatient
