#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to call 01_prepare_sample.sh and 02_run_snakemake.sh when runing PhyloWGS with Snakefile_wes.
#####################################################

set -e
set -u


wdir=$1 # The working directory that contains output for each patient
fpatients=$2  # The list of patients to run
size=$3 # The number of SSMs
all=$4  # Whether or not to filter SSMs in regions with atypical copy numbers
sdir=$5 # The directory containing the scripts
sfile=$6  # The Snakefile to use
fgroup=$7 # A file with three columns for all patients: patient, type(N/T), sample. This is used to distinguish normal and tumor samples
seed=$8 

# Example values of the arguments
# wdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs
# fpatients=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/titan/patients59_wes2
# size=500
# all=Y
# sdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/scripts
# sfile=Snakefile_wes
# fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_wes

if [ $all == "Y" ]; then
  # The single quotes around $region is critical to keep the string grouped together
  region='"--regions all"'
  suffix=wes_all  # The name of output folder     
else
  region=""
  suffix=wes_default  
fi

while read patient; do
  echo $patient
  fsample=$wdir/patient_$patient/config/sample.yaml
  if [ ! -f $fsample ]; then
    # Use the solutions of TitanCNA that are corrected by using ploidy predicted by Sequenza
    bash $sdir/01_prepare_sample.sh $wdir $patient "Y" $fgroup
  fi
  # The double quotes around $region and $seed are required to group the string
  bash $sdir/02_run_snakemake.sh $wdir $patient $sfile WES $size "$region" $suffix "$seed"
  # Use the command below to unlock a failed run of snakemake
  # bash $sdir/unlock_snakemake.sh $wdir $patient $sfile WES $size "$region" $suffix "$seed"
done < $fpatients

