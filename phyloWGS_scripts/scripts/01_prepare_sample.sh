#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to prepare sample.yaml for running PhyloWGS
#####################################################

set -e
set -u

# Revise the directory accordingly
# The directory that stores the results of TitanCNA for all patients
bdir_titan=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/titan
# The directory that stores the results of Mutect for all patients
dir_vcf=/mnt/projects/zhaiww1/planet/DNA/liver_tcr_hch/all_sequenced_so_far_20180412/mutect_results

wdir=$1   # The directory that stores the results of PhyloWGS for all patients 
patient=$2    # The ID of the patient to analyze
corrected=$3    # Whether to use the solutions of TitanCNA that are corrected by using ploidy predicted by Sequenza (Y or N)
# A file with three columns for all patients: patient, type(N/T), sample. This is used to distinguish normal and tumor samples
fgroup=$4

# Example values of the arguments
# wdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs
# patient=PLANET_C002
# corrected=Y
# fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_wgs

gname=patient_"$patient"

# Get results of titan 
dir_titan=$bdir_titan/$gname
if [ $corrected == "Y" ]; then
  res_titan=$dir_titan/results/titan/hmm/optimalClusterSolution_corrected.txt
else
  res_titan=$dir_titan/results/titan/hmm/optimalClusterSolution.txt
fi

# Creating the directory to put configuration files
odir=$wdir/$gname/config
if [ ! -d $odir ]; then
  mkdir -p $odir
fi


# Creating a file with three columns for the patient: patient, type(N/T), sample. This is used to distinguish normal and tumor samples
plist=$wdir/"$gname"/config/patient_sample_list
# A file containing samples for the selected patient (one column with sample names)
fsample=$bdir_titan/$gname/sample_"$patient"
# echo $fsample
# echo $fgroup
echo "Creating file patient_sample_list"
grep -f $fsample $fgroup > $plist


# Write the configuration file for the samples
fyaml=$odir/"sample.yaml"
cat /dev/null > $fyaml
echo "patient_sample_list: $plist" > $fyaml

echo "samples:" >> $fyaml
perl -e 'open CNV, $ARGV[3]; while(<CNV>){@fields=split(/\t/,$_); $path=$fields[10]; chomp $path; $h{$fields[2]}=$path;} open MAP,$ARGV[0]; while(<MAP>){@map=split; $patient=$map[0];  if($map[1] ne "N") {$sample=$map[2]; $vcf="$ARGV[1]/$sample/mutect.PASS.vcf.gz"; $seg="$ARGV[2]/$h{$sample}.segs.txt"; if($h{$sample}) {print " $sample:\n  patient: $patient\n  vcf: $vcf\n  seg: $seg\n";}}}' $plist $dir_vcf $dir_titan $res_titan >> $fyaml

echo "patients:" >> $fyaml
perl -e 'open CNV, $ARGV[1]; while(<CNV>){@fields=split(/\t/,$_); $path=$fields[10]; chomp $path; $h{$fields[2]}=$path;} open MAP,$ARGV[0]; $prev_patient=""; $ssm=""; while(<MAP>){@map=split; $patient=$map[0];  if($prev_patient ne $patient) {if($ssm ne ""){ $samples=substr($samples, 0, -1); print "  samples: $samples\n  ssm: $ssm\n  cnv: $cnv\n  type: $type\n";} print " $patient:\n"; $samples=""; $ssm=""; $cnv=""; $type="";}; $prev_patient=$patient; if($map[1] ne "N") {$sample=$map[2]; if($h{$sample}) { $ssm=$ssm.$sample."=input/".$patient."/".$sample."_ssm.txt "; $cnv=$cnv."--cnvs ".$sample."=input/".$patient."/".$sample."_cnv.txt ";$type=$type."--vcf-type $sample=mutect_smchet ";$samples=$samples."$sample,";}}} $samples=substr($samples, 0, -1); print "  samples: $samples\n  ssm: $ssm\n  cnv: $cnv\n  type: $type";' $plist $res_titan >> $fyaml

## The code block below is for not using CNVs.
# echo "samples:" >> $fyaml
# perl -e 'open MAP,$ARGV[0]; while(<MAP>){@map=split; $patient=$map[0];  if($map[1] ne "N") {$sample=$map[2]; $vcf="$ARGV[1]/$sample/mutect.PASS.vcf.gz"; {print " $sample:\n  patient: $patient\n  vcf: $vcf\n";}}}' $plist $dir_vcf >> $fyaml
# echo "patients:" >> $fyaml
# perl -e 'open MAP,$ARGV[0]; $prev_patient=""; $ssm=""; while(<MAP>){@map=split; $patient=$map[0];  if($prev_patient ne $patient) {if($ssm ne ""){ $samples=substr($samples, 0, -1); print "  samples: $samples\n  ssm: $ssm\n  type: $type\n";} print " $patient:\n"; $samples=""; $ssm=""; $type="";}; $prev_patient=$patient; if($map[1] ne "N") {$sample=$map[2]; { $ssm=$ssm.$sample."=input/".$patient."/".$sample."_ssm.txt "; $type=$type."--vcf-type $sample=mutect_smchet ";$samples=$samples."$sample,";}}} $samples=substr($samples, 0, -1); print "  samples: $samples\n  ssm: $ssm\n  type: $type";' $plist >> $fyaml

