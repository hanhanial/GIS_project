#!/usr/bin/env python

'''
This script is used to reformat the VCF files for diffrent samples of a patient:
1. Moving tumor sample to the last column
2. Subsampling SNVs randomly (optional)
3. Exluding SNVs with low frequency (optional, VAF < 0.05 by default)
'''

from __future__ import division
import argparse
import gzip
import random


def read_type(infile):
    '''
    Read types of samples.
    Format of input file: tab delimited without header --
    Patient, Type (N/T), Sample
    '''
    normal_samples=set()
    with open(infile,'r') as fin:
        for line in fin:
            fields = line.strip().split('\t')
            type = fields[1]
            sample = fields[2]
            if type == "N":
                normal_samples.add(sample)
    return normal_samples


def count_snvs(snv_file, is_gzipped):
    total = 0
    if is_gzipped:
        fin = gzip.open(snv_file,'rt')
    else:
        fin = open(snv_file,'r')
    for line in fin:
        if not line.startswith('#'):
            total += 1
    return total


def reformat_vcf(snv_file, is_gzipped, outfile, normal_samples, to_subsample, include_nonauto, min_vaf):
    '''
    Reformat the input VCF file (a pair of normal-tumor sample).
    Assumption: Only 2 samples. Use the provided sample types
    Output: Move tumor sample to the last column. Simplify column INFO
    '''
    # assert normal_samples is not None
    MAX_SSM = 5000

    if is_gzipped:
        fin = gzip.open(snv_file,'rt')
    else:
        fin = open(snv_file,'r')
    fout = open(outfile,'w')
    tumor_index = 10    # By default tumor data is at the last column
    snv_list = list()
    for line in fin:
        if line.startswith('##'):
            fout.write(line)
            continue
        if line.startswith('#'):
            headers = line.strip().split('\t')
            sample1 = headers[-2]
            if "_" in sample1:
                sample1 = sample1.split("_")[-1]
            sample2 = headers[-1]
            if "_" in sample2:
                sample2 = sample2.split("_")[-1]
            nline = headers[0:-2]
            if normal_samples is not None:
                if sample1 in normal_samples:
                    type1 = "normal"
                    type2 = "tumor"
                    nline.extend([sample1, sample2])
                else:
                    type1 = "tumor"
                    type2 = "normal"
                    nline.extend([sample2, sample1])
                    tumor_index = 9
                print('Sample at the second last column: {} ({})\nSample at the last column: {} ({})'.format(headers[-2], type1, headers[-1], type2))
            else:
                nline.extend([sample1, sample2])
            # Revised header
            wline='\t'.join(nline)  + "\n"
            fout.write(wline)
            continue
        # The data lines
        fields = line.strip().split('\t')
        chrom = fields[0]
        if 'M' in chrom:
            continue
        if not include_nonauto:
            if chrom == 'X' or chrom == 'Y':
                continue
        # Assumed genotye: GT:AD:BQ:DP:FA:SS
        genotye = fields[tumor_index]   
        # AD is the second one
        allele_depth = genotye.split(':')[1].split(',')
        ref_counts = int(allele_depth[0])
        var_counts = int(allele_depth[1])             
        vaf = var_counts / (var_counts + ref_counts)
        if vaf < min_vaf:
            print(" SNV {}:{} excluded due to VAF below {} (SNV can't be explained by an SP present in {}% or more of the sample).".format(fields[0], fields[1], min_vaf, min_vaf * 2))
            continue        
        info = fields[-4].split(";")[0]
        if tumor_index == 9:
            normal = fields[-1]
            tumor = fields[-2]
        else:
            normal = fields[-2]
            tumor = fields[-1]
        # Computue the variant allele frequency to exclude low-frequency variants 
        
        # The first several columns: CHROM  POS     ID      REF     ALT     QUAL    FILTER
        nline=fields[0:-4]
        nline.extend([info, fields[-3], normal, tumor])
        wline='\t'.join(nline) + "\n"
        if to_subsample:
            snv_list.append(wline)
        else:
            fout.write(wline)

    if to_subsample:
        num_ssm = 0
        while num_ssm < MAX_SSM:
            r = int(random.uniform(0, len(snv_list) - 1))
            fout.write(snv_list[r])
            del snv_list[r]
            num_ssm += 1

    fin.close()
    fout.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__,                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-m", dest="snv_file", default="", help="The output of Mutect (e.g. mutect.PASS.vcf)")
    parser.add_argument("-t", dest="type_file", default="", help="The file specifying the type of each sample (normal or tumor)")
    parser.add_argument("-g", dest="is_gzipped", action="store_true", default=False, help="The SNV file is gzipped or not")
    parser.add_argument("-n", dest="include_nonauto", action="store_true", default=False, help="Including non-autosomes (chrX, chrY)")    
    parser.add_argument("-f", dest="min_vaf", type=float, default=0.05, help="The threshold to ignore low-frequency variants")
    parser.add_argument("-s", dest="to_subsample", action="store_true", default=False, help="Subsample SNVs to a maximum of 5000 per sample")
    parser.add_argument("-o", dest="outfile", default="", help="The output file (e.g. WHT195.tsv)")
    args = parser.parse_args()

    if args.type_file:
        normal_samples = read_type(args.type_file)
    else:
        normal_samples = None
    reformat_vcf(args.snv_file, args.is_gzipped, args.outfile, normal_samples, args.to_subsample, args.include_nonauto, args.min_vaf)

