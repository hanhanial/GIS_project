#!/usr/bin/env Rscript

# This script is used to run MapScape from the output of PhyloWGS.

library(mapscape)
library(optparse)
library(htmlwidgets)


run_mapscape <- function(wdir, img_ref){
  clonal_prev <- read.delim(file.path(wdir, "max_clone_freq.txt"))
  tree <- read.delim(file.path(wdir, "max.adj"))
  samples <- read.delim(file.path(wdir, "all_sample.txt"))
  # x = sample(50:150, nrow(samples))
  # y = sample(50:150, nrow(samples))
  interval = 100/nrow(samples)
  x=list()
  y=list()
  for (i in 1:nrow(samples))
  {
    # The samples are in a horizontal line
    # The direction of each line connecting to the sample is random in mapscape()
    x[[i]]=50+interval*(i-1)
    y[[i]]=100
  }
  
  samples$x=x
  samples$y=y
  mutations <- read.delim(file.path(wdir, "max_mutations.txt"))

  mapscape(clonal_prev = clonal_prev, tree_edges = tree,
           sample_locations = samples, img_ref = img_ref, mutations = mutations, show_low_prev_gtypes = TRUE)
}


option_list = list(
  make_option(c("-p", "--patient"), type="character", default="",
                          help="The ID of the patient"),
  make_option(c("-s", "--suffix"), type="character", default="",
                          help="The suffix of the output file"),
  make_option(c("-i", "--img"), type="character", default="/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/config/squares.png",
                          help="The reference image"),
  make_option(c("-d", "--directory"), type="character", default="",
                          help="The working directory which stores the input of MapScape")
            );

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

img_ref=opt$img
patient=opt$patient
wdir=normalizePath(opt$directory)
suffix=opt$suffix

# patient="PLANET_C002"
# wdir="/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/patient_PLANET_C002/WES/default/output/PLANET_C002"
# suffix="wes"
# img_ref="/mnt/projects/alvarezjjs/lungtcr/khipin/Progress_Study/downstream/clonality/2018-6-26_clones_tree_ITH2_liver/squares.png"

p=run_mapscape(wdir, img_ref)
# print(p)
fname=paste0("mapscape_results_", patient, "_", suffix, ".html")
fhtml=file.path(wdir, fname)
htmlwidgets::saveWidget(p, fhtml)

#fname=paste0("mapscape_results_", patient, "_", suffix, ".png")
#fpng=file.path(wdir, fname)
#url = sprintf("file:///%s", fhtml)
#webshot::webshot(url= url, file=fpng)
