#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is used to extract the clonal phylogeny from the pdf file (2nd page) generated from MapScape output, which is then used for combined plots.
#####################################################


# For PhyloWGS output
idir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV
cd $idir

if [ ! -d $idir/mapscape_pdf ];then
  mkdir -p $idir/mapscape_pdf
fi

for file in `ls mapscape_pdf_all`; do
  fin=$idir/mapscape_pdf_all/$file
  fout=$idir/mapscape_pdf/$file
  pdfjam --landscape $fin 2 -o $fout -q --trim '0.9cm 1.15cm 5cm -0.1cm' --clip true
  # pdftk $fin  cat 2  output $fout 
done

# Test on one file
# # --trim '1cm 2cm 5cm 0.8cm'
# patient=PLANET_E003
# fin=mapscape_pdf_all/mapscape_results_"$patient"_ssm_500_all.pdf
# fout=mapscape_pdf/mapscape_results_"$patient"_ssm_500_all.pdf
# pdfjam --landscape $fin 2 -o $fout -q  --trim '0.9cm 1.15cm 5cm -0.1cm' --clip true

