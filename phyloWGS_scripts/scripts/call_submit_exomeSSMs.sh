#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is a wrapper that calls submit_exomeSSMs.sh.
#####################################################

set -e
set -u

# Change the parameters accordingly
# Usually only $pfile is needed to change to run on diffrent sets of patients 
# The list of patients to run
pfile=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/titan/patients59_wes
# The output directory
dir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV
# The directory containing the scripts
sdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/scripts
sfile=Snakefile_exomeSSMs
# For patients with WGS data
# fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_wgs
# For patients with only WES data
fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_wes
# For all patients 
# fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_59
# The single quotes around $seed is critical to keep the string grouped together
# seed='"123 456 789 101112"' # This seed was used to run 16-3267 and PLANET_B003 (without using CNV data)
seed='"12 34 56 78"'

bash $sdir/submit_exomeSSMs.sh $dir $pfile 0 Y $sdir $sfile $fgroup "$seed" 






