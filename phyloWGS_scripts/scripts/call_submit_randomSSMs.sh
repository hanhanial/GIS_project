#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This script is a wrapper that calls submit_randomSSMs.sh.
#####################################################

set -e
set -u

# Change the parameters accordingly
# Usually only $pfile is needed to change to run on diffrent sets of patients 
# The list of patients to run
# pfile=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patients57
pfile=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV/prerun
#pfile=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV/prerun2
# The output directory
dir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/rerun_with_filteredSNV
# The directory containing the scripts
sdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/scripts
sfile=Snakefile_randomSSMs
# For patients with WGS data
fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_wgs
# For all patients 
# fgroup=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/sample/patient_sample_list_59
# The single quotes around $seed is critical to keep the string grouped together
# seed='"123 456 789 101112"' # This seed was used to run 16-3267 and PLANET_B003 (without using CNV data)
seed='"12 34 56 78"'  # This seed was used to run 15-10541, 15-29154, 16-003799, PLANET_D002, 16-3267 and PLANET_B003
# 1 is the default seed 
ssmseed=1
# The subsequent seeds were used rerun failed patients on diffrent sets of SSMs
#ssmseed=123
#ssmseed=12345
#ssmseed=123456
#ssmseed=12


# Use `--regions all` when calling script parse_cnvs.py
#bash $sdir/submit_randomSSMs.sh $dir $pfile 500 Y $sdir $sfile $fgroup "$seed" $ssmseed
# Use `--regions normal_and_abnormal_cn` when calling script parse_cnvs.py
#bash $sdir/submit_randomSSMs.sh $dir $pfile 500 N $sdir $sfile $fgroup "$seed" $ssmseed
# Use more variants to avoid filitering out by copy number state
#bash $sdir/submit_randomSSMs.sh $dir $pfile 1000 N $sdir $sfile $fgroup "$seed" $ssmseed
# Use all SSMs
bash $sdir/submit_randomSSMs.sh $dir $pfile all N $sdir $sfile $fgroup "$seed" $ssmseed
