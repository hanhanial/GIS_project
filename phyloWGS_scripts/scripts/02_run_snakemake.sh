#!/bin/bash

#####################################################
# Author: Bingxin Lu
# Description: This file is used to submit the snakemake pipeline to a cluster node after sample.yaml has been prepared.
#####################################################

set -e
#set -u

# source activate /mnt/projects/lub/workspace/anaconda3/envs/phylowgs/
source activate /mnt/projects/zhaiww1/planet/tmp4bingxin/anaconda3/envs/phylowgs/

# Revise the directory accordingly
# The directory contains snakefiles
cdir=/mnt/projects/lailhh/workspace/GIS_project/phyloWGS_scripts/config
#cdir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs/config
jobs=10

# Go to the directory for a patient
dir=$1   # The working directory to store results for all patients
patient=$2  # The patient ID
# Use diffrent snake files according to the context
fsnake=$3   # The Snakefile to use
subdir=$4   # THe subdirectory that stores the results of snakemake run
ssize=$5    # The number of SSMs to use
region=$6   # The region in the genome (--regions all; --regions normal_cn;  --regions normal_and_abnormal_cn)
suffix=$7   # The name of output folder    
seed=$8   # The random seed used to run PhyloWGS 
num_arg=$#
echo "number of arguments: $num_arg"

# seed="123 456 789 101112"
# Example values of the arguments
# dir=/mnt/projects/zhaiww1/planet/tmp4bingxin/results/liver/phylowgs
# patient="15-23864"
# fsnake=Snakefile_sn
# subdir=WGS
# ssize=500
# region="--regions all"
# suffix=ssm_"$ssize"_all

# This directory contains results for the patient
pdir=$dir/patient_"$patient"

# Ensure that input/ is under the main output directory, shared by the subsequent runs with different suffixes
wdir=$dir/patient_"$patient"/$subdir

# Number of MCMC chains to use
# if number of seeds for MCMC changes, adjust this too!
num_chain=5 # ADDED BY HANNAH

# Creating the directory to store running information of the snakemake pipeline
if [ ! -d $wdir/stdout ]
then
  mkdir -p $wdir/stdout
fi

cluster="qsub -V -l h_rss={cluster.mem},h_rt={cluster.time} -pe OpenMP {cluster.n} -o $wdir/stdout/ -e $wdir/stdout/"
if [[ $num_arg > 8 ]]; then
  ssmseed=$9  # The random seed to select a subset of SSMs
  snake="snakemake --rerun-incomplete -k --latency-wait 120 -j $jobs --config num_chain=$num_chain ssize=$ssize region=$region suffix=$suffix seed="$seed" ssmseed=$ssmseed -s $cdir/$fsnake --configfile $pdir/config/sample.yaml --cluster-config $cdir/cluster.yaml --cluster '"$cluster"'"
else
  snake="snakemake --rerun-incomplete -k --latency-wait 120 -j $jobs --config num_chain=$num_chain ssize=$ssize region=$region suffix=$suffix seed="$seed" -s $cdir/$fsnake --configfile $pdir/config/sample.yaml --cluster-config $cdir/cluster.yaml --cluster '"$cluster"'"
fi
echo $snake
qsub -V -pe OpenMP 1 -l h_rss=8G,h_rt=480:00:00 -N phylowgs -wd $wdir -o $wdir/stdout -e $wdir/stdout -b y "$snake"

source deactivate
